import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let headerData = {
      'Content-Type': 'application/json'
    };
    if (this.auth.isSignedIn()) {
      headerData = Object.assign({'Authorization': `Bearer ${this.auth.user.accessToken}`}, headerData)
    }
    request = request.clone({
      setHeaders: headerData
    });
    return next.handle(request);
  }
}
