export class Task {
  userId?: Number;
  id: Number;
  title: String;
  completed: Boolean;
}
