import {Injectable} from '@angular/core';
import {Task} from './models/task.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {environment} from '../../environments/environment';
import {ToasterService} from 'angular2-toaster';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../auth.service';

@Injectable()
export class TaskService {
  private apiUrl = environment.apiUrl;
  private queryParams: any;

  // protected taskList: Task[] = [];
  constructor(private http: HttpClient, private toasterService: ToasterService, private route: ActivatedRoute, private auth: AuthService) {

    this.queryParams = this.stringifyQueryParams();
  }

  getTasks(queryParams?: any): Observable<Task[]> {
    this.queryParams = this.stringifyQueryParams(queryParams);
    return this.http.get<Task[]>(`${this.apiUrl}/tasks${this.queryParams}`)
      .pipe(
        catchError(this.handleError('getTasks', null))
      );
  }

  getTask(taskId: any): Observable<Task> {
    return this.http.get<Task>(`${this.apiUrl}/tasks/${taskId}`)
      .pipe(
        catchError(this.handleError('getTask', null))
      );
  }

  addTask(taskData: String): Observable<Task> {
    return this.http.post<Task>(`${this.apiUrl}/tasks/`, {
      title: taskData,
      completed: false,
      userId: this.auth.user.id
    }).pipe(
      catchError(this.handleError('addTask', null))
    );
  }

  editTask(data): Observable<Task> {
    return this.http.put<Task>(`${this.apiUrl}/tasks/${data.id}`, data)
      .pipe(
        catchError(this.handleError('editTask', null))
      );
  }

  deleteTask(taskId): Observable<Task> {
    return this.http.delete<any>(`${this.apiUrl}/tasks/${taskId}`)
      .pipe(
        catchError(this.handleError('deleteTask', null))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      this.toasterService.pop('error', operation, error.message);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private stringifyQueryParams(objectParams?: Object) {
    let params = objectParams || this.route.snapshot.queryParams;
    if (this.auth.isSignedIn()) {
      params = Object.assign({'userId': this.auth.user.id}, params);
    }
    return '?' + Object.keys(params).map(key => key + '=' + params[key]).join('&');
  }
}
