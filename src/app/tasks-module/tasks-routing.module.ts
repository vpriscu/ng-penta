import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TaskEditComponent} from './task-edit/task-edit.component';
import {TaskListComponent} from './task-list/task-list.component';

const routes: Routes = [
  {path: '', component: TaskListComponent},
  {path: 'edit/:id', component: TaskEditComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TasksRoutingModule {
}
