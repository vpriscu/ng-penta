import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TaskListComponent} from './task-list/task-list.component';
import {TaskDetailComponent} from './task-detail/task-detail.component';
import {TaskEditComponent} from './task-edit/task-edit.component';
import {ReactiveFormsModule} from '@angular/forms';
import {TasksRoutingModule} from './tasks-routing.module';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import { PlusKeyAddDirective } from '../plus-key-add.directive';


@NgModule({
  imports: [
    CommonModule,
    TasksRoutingModule,
    ReactiveFormsModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  declarations: [
    TaskListComponent,
    TaskDetailComponent,
    TaskEditComponent,
    PlusKeyAddDirective,
  ]
})
export class TasksModule {
}
