import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute} from '@angular/router';
import {TaskService} from '../task.service';
import {Task} from '../models/task.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.scss'],
  providers: [TaskService]
})
export class TaskEditComponent implements OnInit {

  currentTask: Task;
  editForm: FormGroup;

  constructor(private title: Title,
              private taskService: TaskService,
              private route: ActivatedRoute,
              private fb: FormBuilder) {
    this.title.setTitle('Task detail page');

    this.editForm = this.fb.group({
      title: ['', Validators.required],
      completed: '',
    });
  }

  ngOnInit() {
    this.taskService.getTask(this.route.snapshot.paramMap.get('id')).subscribe(data => {
      this.currentTask = data;
      this.title.setTitle(this.title.getTitle() + ': Edit ' + data.id);
      this.editForm.setValue({'completed': data.completed, 'title': data.title});
    });
  }

  onSubmit() {
    this.taskService.editTask(this.currentTask).subscribe();
  }
}
