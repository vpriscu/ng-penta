import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Task} from '../models/task.model';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.scss']
})
export class TaskDetailComponent implements OnInit {

  @Input() data: Task;
  @Output() deleteEmit = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  delete(id) {
    this.deleteEmit.emit(id);
  }
}
