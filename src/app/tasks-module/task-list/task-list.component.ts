import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TaskService} from '../task.service';
import {Task} from '../models/task.model';
import 'rxjs/add/operator/map';
import {FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';
import {fancySearch} from '../../animations';
import {ActivatedRoute} from '@angular/router';
import {ErrorMessagesU} from '../../error-messages';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss'],
  providers: [TaskService],
  animations: [
    fancySearch
  ]
})

export class TaskListComponent implements OnInit {

  tasks: Task[];
  formVisible: String = 'inactive';
  addFormGroup: FormGroup;
  @ViewChild('addFieldInput') addFieldInput: ElementRef;

  constructor(
    private taskService: TaskService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private errorMessage: ErrorMessagesU) {
    this.addFormGroup = fb.group({
      addField: new FormControl('', {
        validators: [Validators.required],
        updateOn: 'blur'
      }),
    });
    this.errorMessage.frm = this.addFormGroup;
  }

  ngOnInit() {
    this.route.queryParams.subscribe(q => {
      this.taskService.getTasks(q).subscribe(data => {
        this.tasks = data;
      });
    });
  }

  add(content) {
    if (this.addFormGroup.valid) {
      this.taskService.addTask(this.addFormGroup.controls.addField.value).subscribe(data => {
        this.tasks.unshift(data);
        this.addFormGroup.reset();
        this.formVisible = 'inactive';
      });
    }
  }

  getErrorMessage(fieldName) {
    return this.errorMessage.getErrorMessage(fieldName);
  }

  delete(task: Number): void {
    this.tasks = this.tasks.filter(h => h.id !== task);
    this.taskService.deleteTask(task).subscribe();
  }

  toggleFormVisibility() {
    this.formVisible = (this.formVisible === 'active') ? 'inactive' : 'active';
    this.addFieldInput.nativeElement.focus();
  }
}
