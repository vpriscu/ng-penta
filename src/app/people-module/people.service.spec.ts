import { TestBed, inject } from '@angular/core/testing';

import { PeopleService } from './people.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('PeopleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PeopleService],
      schemas: [NO_ERRORS_SCHEMA],
    });
  });

  it('should be created', inject([PeopleService], (service: PeopleService) => {
    expect(service).toBeTruthy();
  }));
});
