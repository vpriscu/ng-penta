export class User {
  id: Number;
  name: String;
  username: String;
  email: String;
  address: UserAddress;
}

class UserAddress {
  street: String;
  suite: String;
  city: String;
  zipcode: String;
  geo: UserAddressGeolocation;
}

class UserAddressGeolocation {
  lat: String;
  long: String;
}
