import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PeopleListComponent} from './people-list/people-list.component';
import {PeopleDetailComponent} from './people-detail/people-detail.component';

const routes: Routes = [
  {
    path: '',
    component: PeopleListComponent,
  },
  {
    path: 'edit/:id',
    component: PeopleDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeopleRoutingModule {
}
