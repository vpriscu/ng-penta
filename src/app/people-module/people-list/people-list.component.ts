import { Component, OnInit } from '@angular/core';
import {PeopleService} from '../people.service';
import {User} from '../models/user.model';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.scss'],
  providers: [PeopleService]
})
export class PeopleListComponent implements OnInit {

  users: Observable<User[]>;
  constructor(private userService: PeopleService) { }

  ngOnInit() {
    this.users = this.userService.users;

    this.userService.getAll();
  }

}
