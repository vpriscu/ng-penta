import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {User} from './models/user.model';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {ToasterService} from 'angular2-toaster';
import {Subject} from 'rxjs/Subject';
import {ActivatedRoute} from '@angular/router';

@Injectable()
export class PeopleService {

  private _users: Subject<User[]>;
  users: Observable<User[]>;
  private dataCache: {
    users: User[]
  };
  private apiUrl = environment.apiUrl;
  private queryParams: String;

  constructor(private http: HttpClient, private toasterService: ToasterService, private route: ActivatedRoute) {
    this.dataCache = {users: []};
    this._users = <Subject<User[]>>new Subject;
    this.users = this._users.asObservable();
    const params = this.route.snapshot.queryParams;
    this.queryParams = '?' + Object.keys(params).map(key => key + '=' + params[key]).join('&');

  }

  getAll() {
    this.http.get<User[]>(`${this.apiUrl}/users${this.queryParams}`).subscribe(data => {
      this.dataCache.users = data;
      this.pushToSubject();
    }, error => {
      this.toasterService.pop('error', 'getAll', error.message);
    });
  }

  getOne(id: number | String) {
    this.http.get(`${this.apiUrl}/users/${id}`).subscribe(data => {
      let notFound = true;
      const results: any = data;
      const mapItem: any = this.dataCache.users;

      this.dataCache.users.map((value, index) => {
        if (value.id === results.id) {
          mapItem[index] = data;
          notFound = false;
        }
      });
      if (notFound) {
        this.pushToCache(data);
      }
      this.pushToSubject();
    });
  }

  create(item: User) {
    this.http.post(`${this.apiUrl}/users`, item).subscribe(data => {
      this.pushToCache(data);
      this.pushToSubject();
    });
  }

  update(item: User) {
    this.http.put(`${this.apiUrl}/users/${item.id}`, item).subscribe(data => {
      // for lint to play nice.
      const results: any = data;
      const mapItem: any = this.dataCache.users;
      this.dataCache.users.map((value, index) => {
        if (value.id === results.id) {
          mapItem[index] = data;
        }
      });
      this.pushToSubject();
    });
  }

  remove(id) {
    this.http.delete(`${this.apiUrl}/users/${id}`).subscribe(data => {
      // for lint to play nice.
      const results: any = data;

      this.dataCache.users.map((value, index) => {
        if (value.id === results.id) {
          this.dataCache.users.splice(index, 1);
        }
      });
    });
  }

  private pushToSubject() {
    this._users.next(Object.assign({}, this.dataCache).users);
  }

  private pushToCache(data) {
    this.dataCache.users.push(data);
  }
  private setCache(data) {
    this.dataCache.users = data;
  }
}
