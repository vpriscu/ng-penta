import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PeopleListComponent} from './people-list/people-list.component';
import {PeopleDetailComponent} from './people-detail/people-detail.component';
import {PeopleRoutingModule} from './people-routing.module';

@NgModule({
  imports: [
    CommonModule,
    PeopleRoutingModule
  ],
  declarations: [
    PeopleListComponent,
    PeopleDetailComponent
  ]
})
export class PeopleModule {
}
