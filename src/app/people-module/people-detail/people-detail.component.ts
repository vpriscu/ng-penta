import {Component, OnInit} from '@angular/core';
import {PeopleService} from '../people.service';
import {ActivatedRoute} from '@angular/router';
import {User} from '../models/user.model';

@Component({
  selector: 'app-people-detail',
  templateUrl: './people-detail.component.html',
  styleUrls: ['./people-detail.component.scss'],
  providers: [PeopleService]
})
export class PeopleDetailComponent implements OnInit {
  user: User;
  constructor(
    private userService: PeopleService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.userService.users.subscribe(data => {
      this.user = data[0];
    });
    this.userService.getOne(this.route.snapshot.paramMap.get('id'));
  }

}
