import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {PeopleDetailComponent} from './people-detail.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {PeopleService} from '../people.service';
import {HttpClient} from 'selenium-webdriver/http';

// Somehow this should be used to return fake data.
class MockHttpClient {
  get() {
    console.log('get http');
  }

  post() {
    console.log('post http');
  }
}

describe('PeopleDetailComponent', () => {
  let component: PeopleDetailComponent;
  let fixture: ComponentFixture<PeopleDetailComponent>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [PeopleDetailComponent],
      providers: [
        PeopleService,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeopleDetailComponent);
  });

  it('should create', () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
