import {Injectable} from '@angular/core';
import {User} from './user.model';
import {environment} from '../environments/environment';
import {HttpClient} from '@angular/common/http';
import {ToasterService} from 'angular2-toaster';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthService {

  user: User;
  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient, private toasterService: ToasterService, private router: Router) {
    this.doSignOut();
  }

  public isSignedIn() {
    return !!this.user.accessToken;
  }

  public doSignOut() {
    this.user = new User();
  }

  public signIn(postData) {
    return this.http
      .post(this.apiUrl + '/auth/login', postData)
      .subscribe((data: User) => {
        this.user.accessToken = data.accessToken;
        this.user.id = data.id;
        this.router.navigate(['/']);
      }, error => {
        this.toasterService.pop('error', 'signIn', error.message);
      });
  }

}
