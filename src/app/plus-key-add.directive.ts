import {Directive, ElementRef, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[appPlusKeyAdd]'
})
export class PlusKeyAddDirective {

  @Output() keyEmit = new EventEmitter();

  constructor(private el: ElementRef) {
  }

  @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    if (event.key === '+') {
      this.keyEmit.emit(true);
    }
  }
}
