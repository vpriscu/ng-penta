import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import {ErrorMessagesU} from '../error-messages';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  public loginForm: FormGroup;

  public isBusy = false;
  public hasFailed = false;
  public showInputErrors = false;

  constructor(
    private auth: AuthService,
    private fb: FormBuilder,
    private errorMessage: ErrorMessagesU
  ) {
    this.loginForm = fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.errorMessage.frm = this.loginForm;
  }

  ngOnInit() {
  }

  public doSignIn() {

    // Make sure form values are valid
    if (this.loginForm.invalid) {
      this.showInputErrors = true;
      return;
    }

    // Reset status
    this.isBusy = true;
    this.hasFailed = false;

    // Grab values from form
    const email = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;

    // Submit request to API
    this.auth.signIn({email, password});
  }

  getErrorMessage(fieldName: string) {
    return this.errorMessage.getErrorMessage(fieldName);
  }

}
