import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {ToasterModule} from 'angular2-toaster';
import {NotFoundComponent} from './not-found/not-found.component';
import {RouterModule, Routes} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {SignInComponent} from './sign-in/sign-in.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AuthService} from './auth.service';
import {RequestInterceptor} from './request.interceptor';
import {MatListModule} from '@angular/material/list';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {ErrorMessagesU} from './error-messages';

const routes: Routes = [
  {
    path: '',
    loadChildren: 'app/my-tasks/my-tasks.module#MyTasksModule',
  },
  {
    path: 'user',
    loadChildren: 'app/people-module/people.module#PeopleModule'
  },
  {
    path: 'tasks',
    loadChildren: 'app/tasks-module/tasks.module#TasksModule'
  },
  {
    path: 'sign-in',
    component: SignInComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    SignInComponent,
  ],
  imports: [
    BrowserModule,
    ToasterModule.forRoot(),
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,

  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true}
    , AuthService,
    ErrorMessagesU
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
