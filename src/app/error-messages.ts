import {FormGroup} from '@angular/forms';

/**
 * generic methode for returning error messages in different modules.
 * on import assign the target form tho frm.
 */
export class ErrorMessagesU {
  frm: FormGroup;
  getErrorMessage(fieldName: string) {
    return this.frm.get(fieldName).hasError('required') ? 'You must enter a value' :
      this.frm.get(fieldName).hasError('email') ? 'Not a valid email' : 'generic erro message';
  }
}
