import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyTasksRoutingModule } from './my-tasks-routing.module';
import {TaskListComponent} from '../tasks-module/task-list/task-list.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {TaskDetailComponent} from '../tasks-module/task-detail/task-detail.component';
import {TaskEditComponent} from '../tasks-module/task-edit/task-edit.component';
import {AuthGuard} from '../auth.guard';
import { MyTasksComponent } from './my-tasks.component';

@NgModule({
  imports: [
    CommonModule,
    MyTasksRoutingModule,
  ],
  declarations: [MyTasksComponent],
  providers: [AuthGuard]
})
export class MyTasksModule { }
