import {animate, state, style, transition, trigger} from '@angular/animations';

export const fancySearch = trigger('fancySearch', [
  state('active', style({
    height: '100%',
    opacity: '1',
  })),
  state('inactive', style({
    height: '0',
    opacity: '0',
  })),
  transition('active <=> inactive', animate('300ms ease-in-out')),
])
